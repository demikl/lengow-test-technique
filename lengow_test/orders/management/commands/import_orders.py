import requests
import pytz
from lxml import etree
from decimal import Decimal
from django.core.management.base import BaseCommand, CommandError
from orders.models import Product, Order
from django.utils.dateparse import parse_date, parse_time, parse_datetime
from django.utils import timezone


class Command(BaseCommand):
    help = 'Add new orders into DB from XML file stored as specified URL'
    missing_args_message = 'Specify the URL of the XML feed to import'

    def add_arguments(self, parser):
        parser.add_argument('url')
        parser.add_argument(
            '--purge',
            action='store_true',
            dest='purge',
            default=False,
            help='Purge all orders and products before importing')

    def handle(self, *args, **options):
        if options['purge']:
            Order.objects.all().delete()
            Product.objects.all().delete()

        try:
            xml_root = fetch_and_parse_xml(options['url'])
        except requests.exceptions.RequestException as err:
            raise CommandError('Cannot fetch from URL: {}'.format(err))
        except etree.LxmlError as err:
            raise CommandError('Feed cannot be parsed: {}'.format(err))

        products = create_products(xml_root)
        Product.objects.bulk_create(products)

        orders = create_orders(xml_root)
        Order.objects.bulk_create(orders)

        self.stdout.write(
            'Successfully imported {} new orders and {} new products'.format(
                len(orders),
                len(products)
            ))
        self.stdout.write(
            'Database has now {} orders and {} products.'.format(
                Order.objects.count(),
                Product.objects.count()
            ))


def fetch_and_parse_xml(url):
    """Fetch XML feed from specified URL, then parse it as a DOM tree"""

    # Fetching
    http_response = requests.get(url)
    http_response.raise_for_status()

    # Parsing
    return etree.fromstring(http_response.content)


# Formatters from XML parsing to model datatype
def fmt_str(s):
    return s


def fmt_int(s):
    return int(s) if s else None


def fmt_dec(s):
    return Decimal(s) if s else None


def fmt_float(s):
    return float(s) if s else None


def fmt_date(s):
    return parse_date(s) if s else None


def fmt_time(s):
    return parse_time(s) if s else None


def fmt_datetime(s):
    return pytz.timezone(timezone.get_current_timezone_name()) \
               .localize(parse_datetime(s), is_dst=None) \
        if s else None


def create_products(xml_root):
    """Iterate over each product in the feed, and create product instances
       for objects not known in database
    """
    products = []
    known_products = set()

    # Mapping entre le nom du champ dans le modele django,
    # le xpath d'acces dans le XML, et le typeur de données
    MAPPING = (
        ('idlengow',            './idLengow',       fmt_str),
        ('idmp',                './idMP',           fmt_str),
        ('sku',                 './sku',            fmt_str),
        ('ean',                 './ean',            fmt_str),
        ('title',               './title',          fmt_str),
        ('category',            './category',       fmt_str),
        ('brand',               './brand',          fmt_str),
        ('url_product',         './url_product',    fmt_str),
        ('url_image',           './url_image',      fmt_str),
        ('order_lineid',        './order_lineid',   fmt_int),
        ('quantity',            './quantity',       fmt_int),
        ('price',               './price',          fmt_dec),
        ('shipping_price',      './shipping_price', fmt_dec),
        ('tax',                 './tax',            fmt_dec),
        ('status',              './status',         fmt_str),
    )

    product_nodes = xml_root.iter("product")
    for product_xml in product_nodes:
        xpatheval = etree.XPathEvaluator(product_xml)

        # Does the product already exists in DB ?
        sku = get_xml_node_value(xpatheval, './sku')
        if sku in known_products or Product.objects.filter(pk=sku).exists():
            known_products.add(sku)
            continue

        product = Product(sku)
        for model_field, xpath, formatter in MAPPING:
            value = get_xml_node_value(xpatheval, xpath)
            setattr(product, model_field, formatter(value))
        products.append(product)
    return products


def create_orders(xml_root):
    """Iterate over each order in the feed, and create order instances
       for objects not known in database
    """
    orders = []

    # Mapping entre le nom du champ dans le modele django,
    # le xpath d'acces dans le XML, et le typeur de donnees
    MAPPING = (
        ('marketplace',                     './marketplace',                        fmt_str),
        ('idflux',                          './idFlux',                             fmt_str),
        ('order_status_marketplace',        './order_status/marketplace',           fmt_str),
        ('order_id',                        './order_id',                           fmt_str),
        ('order_mrid',                      './order_mrid',                         fmt_str),
        ('order_refid',                     './order_refid',                        fmt_str),
        ('order_external_id',               './order_external_id',                  fmt_str),
        ('order_purchase_date',             './order_purchase_date',                fmt_date),
        ('order_purchase_heure',            './order_purchase_heure',               fmt_time),
        ('order_amount',                    './order_amount',                       fmt_dec),
        ('order_tax',                       './order_tax',                          fmt_dec),
        ('order_shipping',                  './order_shipping',                     fmt_dec),
        ('order_commission',                './order_commission',                   fmt_dec),
        ('order_processing_fee',            './order_processing_fee',               fmt_dec),
        ('order_currency',                  './order_currency',                     fmt_str),
        ('order_payment_checkout',          './order_payment/payment_checkout',     fmt_str),
        ('order_payment_status',            './order_payment/payment_status',       fmt_str),
        ('order_payment_type',              './order_payment/payment_type',         fmt_str),
        ('order_payment_date',              './order_payment/payment_date',         fmt_date),
        ('order_payment_heure',             './order_payment/payment_heure',        fmt_time),
        ('order_invoice_number',            './order_invoice/invoice_number',       fmt_int),
        ('order_invoice_url',               './order_invoice/invoice_url',          fmt_str),
        ('billing_society',                 './billing_address/billing_society',    fmt_str),
        ('billing_civility',                './billing_address/billing_civility',   fmt_str),
        ('billing_lastname',                './billing_address/billing_lastname',   fmt_str),
        ('billing_firstname',               './billing_address/billing_firstname',  fmt_str),
        ('billing_email',                   './billing_address/billing_email',      fmt_str),
        ('billing_address',                 './billing_address/billing_address',    fmt_str),
        ('billing_address_2',               './billing_address/billing_address_2',  fmt_str),
        ('billing_address_complement',      './billing_address/billing_address_complement', fmt_str),
        ('billing_zipcode',                 './billing_address/billing_zipcode',    fmt_str),
        ('billing_city',                    './billing_address/billing_city',       fmt_str),
        ('billing_country',                 './billing_address/billing_country',    fmt_str),
        ('billing_country_iso',             './billing_address/billing_country_iso', fmt_str),
        ('billing_phone_home',              './billing_address/billing_phone_home', fmt_str),
        ('billing_phone_office',            './billing_address/billing_phone_office', fmt_str),
        ('billing_phone_mobile',            './billing_address/billing_phone_mobile', fmt_str),
        ('delivery_society',                './delivery_address/delivery_society',  fmt_str),
        ('delivery_civility',               './delivery_address/delivery_civility', fmt_str),
        ('delivery_lastname',               './delivery_address/delivery_lastname', fmt_str),
        ('delivery_firstname',              './delivery_address/delivery_firstname', fmt_str),
        ('delivery_email',                  './delivery_address/delivery_email',    fmt_str),
        ('delivery_address',                './delivery_address/delivery_address',  fmt_str),
        ('delivery_address_2',              './delivery_address/delivery_address_2', fmt_str),
        ('delivery_address_complement',     './delivery_address/delivery_address_complement', fmt_str),
        ('delivery_zipcode',                './delivery_address/delivery_zipcode',  fmt_str),
        ('delivery_city',                   './delivery_address/delivery_city',     fmt_str),
        ('delivery_country',                './delivery_address/delivery_country',  fmt_str),
        ('delivery_country_iso',            './delivery_address/delivery_country_iso', fmt_str),
        ('delivery_phone_home',             './delivery_address/delivery_phone_home', fmt_str),
        ('delivery_phone_office',           './delivery_address/delivery_phone_office', fmt_str),
        ('delivery_phone_mobile',           './delivery_address/delivery_phone_mobile', fmt_str),
        ('tracking_method',                 './tracking_informations/tracking_method',  fmt_str),
        ('tracking_carrier',                './tracking_informations/tracking_carrier', fmt_str),
        ('tracking_number',                 './tracking_informations/tracking_number',  fmt_str),
        ('tracking_url',                    './tracking_informations/tracking_url',     fmt_str),
        ('tracking_shipped_date',           './tracking_informations/tracking_shipped_date', fmt_datetime),
        ('tracking_relay',                  './tracking_informations/tracking_relay',   fmt_str),
        ('tracking_deliveringbymarketplace', './tracking_informations/tracking_deliveringByMarketPlace', fmt_str),
        ('tracking_parcel_weight',          './tracking_informations/tracking_parcel_weight', fmt_float),
        ('order_comments',                  './order_comments',                     fmt_str),
        ('customer_id',                     './customer_id',                        fmt_str),
        ('order_ip',                        './order_ip',                           fmt_str),
    )

    order_nodes = xml_root.iter("order")
    for order_xml in order_nodes:
        xpatheval = etree.XPathEvaluator(order_xml)

        # Does the order already exists in DB ?
        order_id = get_xml_node_value(xpatheval, './order_id')
        if Order.objects.filter(order_id=order_id).exists():
            continue

        order = Order(order_id)
        for model_field, xpath, formatter in MAPPING:
            value = get_xml_node_value(xpatheval, xpath)
            setattr(order, model_field, formatter(value))

        # add product item into order
        product_nodes = order_xml.iter("product")
        for product_xml in product_nodes:
            xpatheval_product = etree.XPathEvaluator(product_xml)
            sku = get_xml_node_value(xpatheval_product, './sku')
            quantity = int(get_xml_node_value(xpatheval_product, './quantity'))
            for _ in range(quantity):
                order.products.add(Product(pk=sku))

        orders.append(order)
    return orders


def get_xml_node_value(xpatheval, xpath):
    """Fetch value stored at [xpath] using [xpathevaluator]"""
    try:
        nodes = xpatheval(xpath)
    except etree.XPathEvalError:
        return None
    assert len(nodes) == 1, "xpath didn't return exactly one node:{} - nodes:{}".format(xpath, nodes)
    value_node = nodes[0]
    if isinstance(value_node, str):
        return value_node.strip()
    else:
        return value_node.text.strip()
