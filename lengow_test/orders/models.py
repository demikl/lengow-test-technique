import django_filters
from django.db import models
from datetime import datetime, time
import pytz


# field template for price-related fields
def price_type():
    return models.DecimalField(
        max_digits=11,
        decimal_places=2,
        default=0,
        null=True)


class Product(models.Model):
    idlengow = models.SlugField()
    idmp = models.SlugField()
    sku = models.SlugField(primary_key=True)
    ean = models.SlugField(blank=True, null=True)
    title = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
    brand = models.CharField(max_length=200, blank=True)
    url_product = models.URLField(blank=True)
    url_image = models.URLField()
    order_lineid = models.SlugField(blank=True, null=True)
    quantity = models.PositiveIntegerField(default=1),
    price = price_type()
    shipping_price = price_type()
    tax = price_type()
    status = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return "{} - {}".format(self.sku, self.title)


class Order(models.Model):
    marketplace = models.CharField(max_length=50)
    idflux = models.PositiveIntegerField()

    MP_ACCEPT = 'accept'
    MP_VALIDATED = 'ValidatedFianet'
    ORDER_STATUS_MP_CHOICES = (
        (MP_ACCEPT, 'Accepted'),
        (MP_VALIDATED, 'Validated by Fianet'),
    )
    order_status_marketplace = models.CharField(
        max_length=15,
        choices=ORDER_STATUS_MP_CHOICES,
        default=MP_ACCEPT)

    def order_status_lengow(self):
        return {
            self.MP_ACCEPT: 'processing',
            self.MP_VALIDATED: 'new'
        }.get(self.order_status_marketplace, 'unknown')

    order_id = models.SlugField(primary_key=True)
    order_mrid = models.SlugField()
    order_refid = models.SlugField()
    order_external_id = models.SlugField(blank=True)

    order_purchase_date = models.DateField(null=True)
    order_purchase_heure = models.TimeField(null=True)

    def order_date(self):
        """ Purchase date in ISO 8601 format """
        if self.order_purchase_date is None:
            return None
        dt = datetime.combine(
            self.order_purchase_date,
            self.order_purchase_heure or time(0, 0, 0)
        )
        api_tz = pytz.timezone("Europe/Paris")
        return api_tz.localize(dt).isoformat()

    order_amount = price_type()
    order_tax = price_type()
    order_shipping = price_type()
    order_commission = price_type()
    order_processing_fee = price_type()
    order_currency = models.CharField(max_length=3, default='EUR')

    order_payment_checkout = models.CharField(max_length=50, blank=True)
    order_payment_status = models.CharField(max_length=50, blank=True)
    order_payment_type = models.CharField(max_length=50, blank=True)
    order_payment_date = models.DateField(null=True)
    order_payment_heure = models.TimeField(null=True)

    order_invoice_number = models.IntegerField(blank=True, null=True)
    order_invoice_url = models.URLField(blank=True)

    billing_society = models.CharField(max_length=50, blank=True)
    billing_civility = models.CharField(max_length=4, blank=True)
    billing_lastname = models.CharField(max_length=50)
    billing_firstname = models.CharField(max_length=50, blank=True)
    billing_email = models.EmailField(blank=True)
    billing_address = models.TextField(blank=True)
    billing_address_2 = models.TextField(blank=True)
    billing_address_complement = models.TextField(blank=True)
    billing_zipcode = models.CharField(max_length=5)
    billing_city = models.CharField(max_length=50)
    billing_country = models.CharField(max_length=3)
    billing_country_iso = models.CharField(max_length=3)
    billing_phone_home = models.CharField(max_length=20, blank=True)
    billing_phone_office = models.CharField(max_length=20, blank=True)
    billing_phone_mobile = models.CharField(max_length=20, blank=True)

    delivery_society = models.CharField(max_length=50, blank=True)
    delivery_civility = models.CharField(max_length=4, blank=True)
    delivery_lastname = models.CharField(max_length=50)
    delivery_firstname = models.CharField(max_length=50, blank=True)
    delivery_email = models.EmailField(blank=True)
    delivery_address = models.TextField(blank=True)
    delivery_address_2 = models.TextField(blank=True)
    delivery_address_complement = models.TextField(blank=True)
    delivery_zipcode = models.CharField(max_length=5)
    delivery_city = models.CharField(max_length=50)
    delivery_country = models.CharField(max_length=3)
    delivery_country_iso = models.CharField(max_length=3)
    delivery_phone_home = models.CharField(max_length=20, blank=True)
    delivery_phone_office = models.CharField(max_length=20, blank=True)
    delivery_phone_mobile = models.CharField(max_length=20, blank=True)

    def get_full_address(self):
        return " ".join([
            self.billing_address,
            self.billing_address_2,
            self.billing_address_complement,
            self.billing_zipcode,
            self.billing_city,
            self.billing_country
        ])

    def billing_full_address(self):
        return self.get_full_address()

    def delivery_full_address(self):
        return self.get_full_address()

    def delivery_full_address_api(self):
        return " ".join([
            self.delivery_address,
            self.delivery_address_2,
            self.delivery_zipcode,
        ])

    tracking_method = models.CharField(max_length=50, blank=True)
    tracking_carrier = models.CharField(max_length=50)
    tracking_number = models.SlugField(blank=True)
    tracking_url = models.URLField(blank=True)
    tracking_shipped_date = models.DateTimeField(blank=True, null=True)
    tracking_relay = models.CharField(max_length=50, blank=True)
    tracking_deliveringbymarketplace = models.NullBooleanField()
    tracking_parcel_weight = models.FloatField(blank=True, null=True)

    order_comments = models.TextField()
    customer_id = models.SlugField()
    order_ip = models.GenericIPAddressField(blank=True, null=True)

    products = models.ManyToManyField(Product)

    def __str__(self):
        return "Order ID {} amount {} {}".format(
            self.order_id, self.order_amount, self.order_currency)


class OrderFilter(django_filters.FilterSet):
    class Meta:
        model = Order
        fields = {
            'order_id': ['icontains'],
            'order_status_marketplace': ['icontains'],
        }
