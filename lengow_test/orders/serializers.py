from rest_framework import serializers, viewsets
from .models import Order, Product


class ProductSerializer(serializers.ModelSerializer):
    """API serializer for Product objects"""
    marketplace_product_id = serializers.SlugField(source='idmp')
    lengow_product_id = serializers.SlugField(source='idlengow')

    class Meta:
        model = Product
        fields = (
            'marketplace_product_id',
            'lengow_product_id',
            'ean',
            'sku',
            'title',
            'brand',
            'price',
        )


class DeliverySerializer(serializers.ModelSerializer):
    """API serializer for delivery address of Order object"""
    lastname = serializers.CharField(source='delivery_lastname')
    firstname = serializers.CharField(source='delivery_firstname')
    address_1 = serializers.CharField(source='delivery_address')
    address_2 = serializers.CharField(source='delivery_address_2')
    zipcode = serializers.CharField(source='delivery_zipcode')
    city = serializers.CharField(source='delivery_city')
    full_address = serializers.CharField(source='delivery_full_address_api')

    class Meta:
        model = Order
        fields = (
            'lastname',
            'firstname',
            'address_1',
            'address_2',
            'zipcode',
            'city',
            'full_address',
        )


class OrderSerializer(serializers.ModelSerializer):
    """API serializer for Order objects"""
    feed_id = serializers.SlugField(source='idflux')
    marketplace_status = serializers.CharField(
            source='order_status_marketplace')
    lengow_status = serializers.CharField(source='order_status_lengow')
    delivery_address = DeliverySerializer(source='*')
    items = ProductSerializer(source='products', many=True)

    class Meta:
        model = Order
        fields = (
            'marketplace',
            'feed_id',
            'marketplace_status',
            'lengow_status',
            'order_date',
            'order_id',
            'order_amount',
            'order_currency',
            'delivery_address',
            'items',
        )
