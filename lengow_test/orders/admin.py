from django.contrib import admin

from .models import Order, Product


class ProductInline(admin.TabularInline):
    model = Order.products.through
    extra = 1


class OrderAdmin(admin.ModelAdmin):
    readonly_fields = (
        'order_status_lengow',
        'billing_full_address',
        'delivery_full_address',
    )
    fieldsets = [
        (None, {'fields': [
            ('marketplace', 'idflux'),
            ('order_id', 'order_mrid', 'order_refid', 'order_external_id'),
            ('order_purchase_date', 'order_purchase_heure'),
            'order_status_marketplace',
            'order_comments',
            'customer_id',
            'order_ip',
        ]}),
        ('Prices', {'fields': [
            'order_amount',
            'order_tax',
            'order_shipping',
            'order_commission',
            'order_processing_fee',
            'order_currency'
        ]}),
        ('Payment', {'fields': [
            'order_payment_checkout',
            'order_payment_status',
            'order_payment_type',
            'order_payment_date',
            'order_payment_heure',
            ],
         'classes': ['collapse']}),
        ('Invoice', {'fields': [
            'order_invoice_number',
            'order_invoice_url',
        ]}),
        ('Billing address', {'fields': [
            'billing_society',
            ('billing_civility', 'billing_lastname', 'billing_firstname'),
            'billing_email',
            'billing_address',
            'billing_address_2',
            'billing_address_complement',
            'billing_zipcode',
            'billing_city',
            'billing_country',
            'billing_country_iso',
            'billing_phone_home',
            'billing_phone_office',
            'billing_phone_mobile',
            ],
         'classes': ['collapse']}),
        ('Delivery address', {'fields': [
            'delivery_society',
            ('delivery_civility', 'delivery_lastname', 'delivery_firstname'),
            'delivery_email',
            'delivery_address',
            'delivery_address_2',
            'delivery_address_complement',
            'delivery_zipcode',
            'delivery_city',
            'delivery_country',
            'delivery_country_iso',
            'delivery_phone_home',
            'delivery_phone_office',
            'delivery_phone_mobile',
            ],
         'classes': ['collapse']}),
        ('Tracking info', {'fields': [
            'tracking_method',
            'tracking_carrier',
            'tracking_number',
            'tracking_url',
            'tracking_shipped_date',
            'tracking_relay',
            'tracking_deliveringbymarketplace',
            'tracking_parcel_weight',
            ],
         'classes': ['collapse']}),
    ]
    inlines = [ProductInline]
    list_display = ('order_id', 'order_amount', 'order_currency',
                    'order_status_lengow')

admin.site.register(Order, OrderAdmin)
admin.site.register(Product)
