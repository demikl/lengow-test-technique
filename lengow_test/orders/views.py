from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views import generic
from rest_framework import viewsets
from .models import Product, Order, OrderFilter
from .serializers import OrderSerializer


def order_list(request):
    """Index page listing all orders with limited filtering capabilities"""
    f = OrderFilter(request.GET)
    paginator = Paginator(f.qs, 25)

    page = request.GET.get('page')
    try:
        orders = paginator.page(page)
    except PageNotAnInteger:
        orders = paginator.page(1)  # first page
    except EmptyPage:
        orders = paginator.page(paginator.num_pages)  # last page if too high
    return render_to_response('orders/order_filter.html', {
                                'object_list': orders,
                                'f': f
                              })


class DetailView(generic.DetailView):
    """Detail page showing various details about an order"""
    model = Order


# API related views #

class OrderViewSet(viewsets.ReadOnlyModelViewSet):
    """API endpoint managing orders"""
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_queryset(self):
        queryset = Order.objects.all()
        status = self.request.query_params.get('status', None)
        if status is not None:
            # get back to marketplace status
            reversed_status = {
                "new": "ValidatedFianet",
                "processing": "accept"
            }.get(status, None)
            if reversed_status is None:
                # no reverse-mapping, so no objects to fetch
                queryset = Order.objects.none()
            else:
                queryset = queryset.filter(
                    order_status_marketplace=reversed_status)
        return queryset
