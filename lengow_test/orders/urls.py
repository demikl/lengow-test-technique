from django.conf.urls import url
from django_filters.views import FilterView
from . import views
from .models import Order

urlpatterns = [
    url(r'^$', views.order_list, name='index'),
    url(r'^filters/$', FilterView.as_view(model=Order), name='filters'),
    url(r'^(?P<pk>[0-9A-Za-z_-]+)/$', views.DetailView.as_view(), name='order_detail'),
]
