# lengow-test-technique #
Candidature Mickael Le Baillif / Test Core Lengow


# Usages : #
* importer un flux de données XML ; on peut ajouter l'option "--purge" pour supprimer, préalablement à l'import, toutes les commandes et produits déjà en BDD

```
#!shell
python manage.py import_orders http://test.lengow.io/orders-test.xml
python manage.py import_orders --purge http://test.lengow.io/orders-test.xml
```


* listing des commandes : http://127.0.0.1:8000/orders/

* API :
    * point d'entrée : http://127.0.0.1:8000/api/orders/
    * filtrage sur statut lengow lors du requetage d'API :
        *        http://127.0.0.1:8000/api/orders/?status=processing
        *        http://127.0.0.1:8000/api/orders/?status=new